--------------------------------------------------------------------------------
-- HEIG-VD
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
--------------------------------------------------------------------------------
-- REDS Institute
-- Reconfigurable Embedded Digital Systems
--------------------------------------------------------------------------------
--
-- File     : Min_Max_top_tb.vhd
-- Author   : TbGenerator
-- Date     : 11.03.2020
--
-- Context  :
--
--------------------------------------------------------------------------------
-- Description : This module is a simple VHDL testbench.
--               It instanciates the DUV and proposes a TESTCASE generic to
--               select which test to start.
--
--------------------------------------------------------------------------------
-- Dependencies : -
--
--------------------------------------------------------------------------------
-- Modifications :
-- Ver   Date        Person     Comments
-- 0.1   11.03.2020  TbGen      Initial version
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity Min_Max_top_tb is
    generic (
        TESTCASE : integer := 0;
        VALSIZE  : integer := 4;
        ERRNO    : integer := 0
    );
    
end Min_Max_top_tb;

architecture testbench of Min_Max_top_tb is

    signal Com_sti  : Std_Logic_Vector ( 1 downto 0 );
    signal Max_sti  : Std_Logic_Vector ( VALSIZE-1 downto 0 );
    signal Min_sti  : Std_Logic_Vector ( VALSIZE-1 downto 0 );
    signal Osc_sti  : std_logic;
    signal Val_sti  : Std_Logic_Vector ( VALSIZE-1 downto 0 );
    signal Leds_obs : std_logic_vector ( 2**VALSIZE-1 downto 0 );
    
    signal sim_end_s : boolean := false;

    component Min_Max_top is
    generic (
        VALSIZE : integer := 4;
        ERRNO   : integer := 0
    );
    port (
        Com_i  : in Std_Logic_Vector ( 1 downto 0 );
        Max_i  : in Std_Logic_Vector ( VALSIZE-1 downto 0 );
        Min_i  : in Std_Logic_Vector ( VALSIZE-1 downto 0 );
        Osc_i  : in std_logic;
        Val_i  : in Std_Logic_Vector ( VALSIZE-1 downto 0 );
        Leds_o : out std_logic_vector ( 2**VALSIZE-1 downto 0 )
    );
    end component;
    

begin

    duv : Min_Max_top
    generic map (
        VALSIZE => VALSIZE,
        ERRNO   => ERRNO
    )
    port map (
        Com_i  => Com_sti,
        Max_i  => Max_sti,
        Min_i  => Min_sti,
        Osc_i  => Osc_sti,
        Val_i  => Val_sti,
        Leds_o => Leds_obs
    );
    

    stimulus_proc: process is
    begin
        -- Com_sti  <= default_value;
        -- Max_sti  <= default_value;
        -- Min_sti  <= default_value;
        -- Osc_sti  <= default_value;
        -- Val_sti  <= default_value;
        

        -- do something
        case TESTCASE is
            when 0      => -- default testcase
            when others => report "Unsupported testcase : "
                                  & integer'image(TESTCASE)
                                  severity error;
        end case;

        -- end of simulation
        sim_end_s <= true;

        -- stop the process
        wait;

    end process; -- stimulus_proc

end testbench;
