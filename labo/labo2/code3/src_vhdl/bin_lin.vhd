--<hdlarch>

-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : bin_lin.vhd
--
-- Description  :
--
-- Auteur       : gilles.habegger
-- Date         : 24.10.2012
-- Version      : 0.0
--
-- Utilise      : Ce fichier est genere automatiquement par le logiciel
--              : \"HDL Designer Series HDL Designer\".
--
--| Modifications |------------------------------------------------------------
-- Version   Auteur Date               Description
--
-------------------------------------------------------------------------------

--<hdlentite>
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity Bin_Lin is
   generic( VALSIZE : integer := 4)	;
   port(
      Val_Bin_i : in     Std_Logic_Vector ( VALSIZE-1 downto 0 );
      Val_Lin_o : out    Std_Logic_Vector ( 2**VALSIZE-1 downto 0 )
   );

-- Declarations

end Bin_Lin ;

--</hdlentite>

architecture behave of Bin_Lin is

begin

process(all) is
begin
	for i in Val_Bin_i'range loop
		if Val_Bin_i(i) /= '0' and Val_Bin_i(i) /= '1' then
			Val_Lin_o <= (others => 'X');
			exit;
		end if;
	end loop;
	for i in Val_Lin_o'range loop
		if (unsigned(Val_Bin_i)) >= i then
			VaL_Lin_o(i) <= '1';
		else
			Val_Lin_o(i) <= '0';
		end if;
	end loop;
  end process;

end behave;
