--<hdlarch>

-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : comparator_8bits.vhd
--
-- Description  : 
-- 
-- Auteur       : gilles.habegger
-- Date         : 24.10.2012
-- Version      : 0.0
-- 
-- Utilise      : Ce fichier est genere automatiquement par le logiciel 
--              : \"HDL Designer Series HDL Designer\".
-- 
--| Modifications |------------------------------------------------------------
-- Version   Auteur Date               Description
-- 
-------------------------------------------------------------------------------

--<hdlentite>
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity Comparator_nbits is
   port( 
      Val_A_i  : in     Std_Logic_Vector;
      Val_B_i  : in     Std_Logic_Vector;
      Result_o : out    std_logic
   );

-- Declarations

end Comparator_nbits ;

--</hdlentite>

architecture comport of Comparator_nbits is

begin -- comport

Result_o <= '1' when 	(Val_A_i > Val_B_i) else
            '0';

end comport;

