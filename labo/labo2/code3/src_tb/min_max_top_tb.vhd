--------------------------------------------------------------------------------
-- HEIG-VD
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
--------------------------------------------------------------------------------
-- REDS Institute
-- Reconfigurable Embedded Digital Systems
--------------------------------------------------------------------------------
--
-- File     : Min_Max_top_tb.vhd
-- Author   : TbGenerator
-- Date     : 11.03.2020
--
-- Context  :
--
--------------------------------------------------------------------------------
-- Description : This module is a simple VHDL testbench.
--               It instanciates the DUV and proposes a TESTCASE generic to
--               select which test to start.
--
--------------------------------------------------------------------------------
-- Dependencies : -
--
--------------------------------------------------------------------------------
-- Modifications :
-- Ver   Date        Person     Comments
-- 0.1   11.03.2020  TbGen      Initial version
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity Min_Max_top_tb is
    generic (
        TESTCASE : integer := 0;
        VALSIZE  : integer := 4;
        ERRNO    : integer := 0
    );

end Min_Max_top_tb;

architecture testbench of Min_Max_top_tb is

    type stimulus_t is record
        Com_i : Std_Logic_Vector ( 1 downto 0 );
        Max_i : Std_Logic_Vector ( VALSIZE-1 downto 0 );
        Min_i : Std_Logic_Vector ( VALSIZE-1 downto 0 );
        Osc_i : std_logic;
        Val_i : Std_Logic_Vector ( VALSIZE-1 downto 0 );
    end record;

    type observed_t is record
        Leds_o : std_logic_vector ( 2**VALSIZE-1 downto 0 );
    end record;


    signal stimulus_sti  : stimulus_t;
    signal observed_obs  : observed_t;
    signal reference_ref : observed_t;

    constant PERIOD : time := 10 ns;

    signal sim_end_s : boolean   := false;
    signal synchro_s : std_logic := '0';

    component Min_Max_top is
    generic (
        VALSIZE : integer := 4;
        ERRNO   : integer := 0
    );
    port (
        Com_i  : in Std_Logic_Vector ( 1 downto 0 );
        Max_i  : in Std_Logic_Vector ( VALSIZE-1 downto 0 );
        Min_i  : in Std_Logic_Vector ( VALSIZE-1 downto 0 );
        Osc_i  : in std_logic;
        Val_i  : in Std_Logic_Vector ( VALSIZE-1 downto 0 );
        Leds_o : out std_logic_vector ( 2**VALSIZE-1 downto 0 )
    );
    end component;


    procedure check(stimulus  : stimulus_t;
                    observed  : observed_t;
                    reference : observed_t) is
    begin
        -- TODO : do the check, maybe differently
        if (observed /= reference) then
            report "Error in check" severity error;
        end if;
    end check;

    procedure calculate_reference(stimulus : stimulus_t;
                                  reference : out observed_t) is
    begin
        -- TODO : calculate the reference

    end calculate_reference;


    procedure testcase0(signal synchro : in std_logic;
                        signal stimulus : out stimulus_t;
                        signal reference : out observed_t) is

        variable stimulus_v  : stimulus_t;
        variable reference_v : observed_t;

    begin
        for i in 0 to 999 loop
            wait until rising_edge(synchro);
            -- TODO : assign the stimulus_v variable
            -- stimulus_v.Com_i := default_value;
            -- stimulus_v.Max_i := default_value;
            -- stimulus_v.Min_i := default_value;
            -- stimulus_v.Osc_i := default_value;
            -- stimulus_v.Val_i := default_value;


            stimulus <= stimulus_v;
            calculate_reference(stimulus_v, reference_v);
            reference <= reference_v;
        end loop;
    end testcase0;

begin

    duv : Min_Max_top
    generic map (
        VALSIZE => VALSIZE,
        ERRNO   => ERRNO
    )
    port map (
        Com_i  => stimulus_sti.Com_i,
        Max_i  => stimulus_sti.Max_i,
        Min_i  => stimulus_sti.Min_i,
        Osc_i  => stimulus_sti.Osc_i,
        Val_i  => stimulus_sti.Val_i,
        Leds_o => observed_obs.Leds_o
    );


    synchro_proc : process is
    begin
        while not(sim_end_s) loop
            synchro_s <= '0', '1' after PERIOD/2;
            wait for PERIOD;
        end loop;
        wait;
    end process;

    verif_proc : process is
        variable reference_v : observed_t;
    begin
        loop
            wait until falling_edge(synchro_s);
            check(stimulus_sti, observed_obs, reference_ref);
        end loop;
    end process;

    stimulus_proc: process is
        variable stimulus_v  : stimulus_t;
        variable reference_v : observed_t;
    begin
        -- stimulus_sti.Com_i <= default_value;
        -- stimulus_sti.Max_i <= default_value;
        -- stimulus_sti.Min_i <= default_value;
        -- stimulus_sti.Osc_i <= default_value;
        -- stimulus_sti.Val_i <= default_value;


        report "Running TESTCASE " & integer'image(TESTCASE) severity note;

        -- do something
        case TESTCASE is
            when 0      => -- default testcase

                testcase0(synchro_s, stimulus_sti, reference_ref);

            when others => report "Unsupported testcase : "
                                  & integer'image(TESTCASE)
                                  severity error;
        end case;

        -- end of simulation
        sim_end_s <= true;

        -- stop the process
        wait;

    end process; -- stimulus_proc

end testbench;
