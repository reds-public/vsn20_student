
# !/usr/bin/tclsh

# Main proc at the end #

#------------------------------------------------------------------------------
proc compile_duv { DUV_FILENAME } {
  # do compile.do

  global Path_DUV
  puts "\nVHDL DUV compilation :"

  vcom -2008 $Path_DUV/math_computer_pkg.vhd
  vcom -2008 $Path_DUV/math_computer_datapath.vhd
  vcom -2008 $Path_DUV/math_computer_control.vhd
  vcom -2008 $Path_DUV/$DUV_FILENAME
}

#------------------------------------------------------------------------------
proc compile_tb { } {
  global Path_TB
  global Path_DUV
  puts "\nVHDL TB compilation :"

  vcom -2008 $Path_TB/transactions_pkg.vhd

  # Input Agent
  vcom -2008 $Path_TB/input_agent/input_agent_driver.vhd
  vcom -2008 $Path_TB/input_agent/input_agent_monitor.vhd
  vcom -2008 $Path_TB/input_agent/input_agent_sequencer.vhd
  vcom -2008 $Path_TB/input_agent/input_agent.vhd

  # Output Agent
  vcom -2008 $Path_TB/output_agent/output_agent_driver.vhd
  vcom -2008 $Path_TB/output_agent/output_agent_monitor.vhd
  vcom -2008 $Path_TB/output_agent/output_agent.vhd

  # Scoreboard
  vcom -2008 $Path_TB/scoreboard.vhd

  # Test Bench
  vcom -2008 $Path_TB/math_computer_tb.vhd
}

#------------------------------------------------------------------------------
proc sim_start { DATASIZE TESTCASE ERRNO} {

  vsim -t 1ns -GDATASIZE=$DATASIZE -GERRNO=$ERRNO -GTESTCASE=$TESTCASE work.math_computer_tb
#  do wave.do
  add wave -r *
  wave refresh
  run -all
}

#------------------------------------------------------------------------------
proc do_all { DUV_FILENAME DATASIZE TESTCASE ERRNO} {
  compile_duv $DUV_FILENAME
  compile_tb
  sim_start $DATASIZE $ERRNO $TESTCASE
}

## MAIN #######################################################################

# Compile folder ----------------------------------------------------
if {[file exists work] == 0} {
  vlib work
}

puts -nonewline "  Path_VHDL => "
set Path_DUV     "../src"
set Path_TB       "../src_tb"

global Path_DUV
global Path_TB

# start of sequence -------------------------------------------------

if {$argc>0} {
  if {[string compare $1 "all"] == 0} {
    do_all math_computer.vhd 8 0 0
  } elseif {[string compare $1 "comp_duv"] == 0} {
    compile_duv $2
  } elseif {[string compare $1 "comp_tb"] == 0} {
    compile_tb
  } elseif {[string compare $1 "sim"] == 0} {
    sim_start $2 $3 $4 $5
  }

} else {
  do_all math_computer.vhd 32 0 0
}
