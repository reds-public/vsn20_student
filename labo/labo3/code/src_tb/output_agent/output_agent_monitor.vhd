library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library tlmvm;
context tlmvm.tlmvm_context;

use work.transactions_pkg.all;
use work.math_computer_pkg.all;

entity output_agent_monitor is
    generic (
        DATASIZE : integer := 8;
        procedure blocking_put(variable output_transaction : in output_transaction_t)
        );
    port (
        signal clk         : in std_logic;
        signal rst         : in std_logic;
        signal port_input  : in math_output_itf_in_t;
        signal port_output : in math_output_itf_out_t
        );
end output_agent_monitor;


architecture testbench of output_agent_monitor is

begin
    process is

        variable transaction : output_transaction_t(r(DATASIZE-1 downto 0));
        variable counter     : integer;
        variable ok          : boolean;
    begin

        counter := 0;
        for i in 0 to 9 loop
            report "Monitor waiting for transaction number " & integer'image(counter) severity note;
            ok := false;
            while (not ok) loop
                wait until rising_edge(clk);
                -- TODO : Handle the protocol correctly
                
                transaction.r := port_output.result;
                blocking_put(transaction);
                report "Monitor received transaction number " & integer'image(counter) severity note;
                counter       := counter + 1;
                ok            := true;
            end loop;
        end loop;

        wait;
    end process;

end testbench;
