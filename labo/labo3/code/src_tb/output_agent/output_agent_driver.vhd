library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library tlmvm;
context tlmvm.tlmvm_context;

use work.transactions_pkg.all;
use work.math_computer_pkg.all;

entity output_agent_driver is
    generic (
        TESTCASE : integer := 0
        );
    port (
        signal clk         : in  std_logic;
        signal rst         : in  std_logic;
        signal port_input  : out math_output_itf_in_t;
        signal port_output : in  math_output_itf_out_t
        );
end output_agent_driver;

architecture testbench of output_agent_driver is

begin

    process is
    begin
        -- TODO : Act correctly, maybe by puttint ready to '0' sometime...
        --        Maybe depending on the testcase...
        port_input.ready <= '1';
        wait;
    end process;

end testbench;
