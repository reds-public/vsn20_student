library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library tlmvm;
context tlmvm.tlmvm_context;

use work.transactions_pkg.all;
use work.math_computer_pkg.all;

entity output_agent is
    generic (
        DATASIZE : integer := 8;
        TESTCASE : integer := 0;
        procedure blocking_put(variable output_transaction : in output_transaction_t)
        );
    port (
        signal clk_i         : in  std_logic;
        signal rst_i         : in  std_logic;
        signal port_input_o  : out math_output_itf_in_t;
        signal port_output_i : in  math_output_itf_out_t
        );
end output_agent;

architecture testbench of output_agent is

    signal port_input_s : math_output_itf_in_t;

begin

    port_input_o <= port_input_s;

    output_agent_driver : entity work.output_agent_driver
        generic map (
            TESTCASE => TESTCASE
            )
        port map (clk_i,
                  rst_i,
                  port_input_s,
                  port_output_i);

    output_agent_monitor : entity work.output_agent_monitor
        generic map(
            DATASIZE     => DATASIZE,
            blocking_put => blocking_put
            )
        port map(
            clk_i,
            rst_i,
            port_input_s,
            port_output_i);

end testbench;
