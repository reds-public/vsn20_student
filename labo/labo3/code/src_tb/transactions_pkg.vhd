library ieee;
use ieee.std_logic_1164.all;

package transactions_pkg is

    type input_transaction_t is record
        a : std_logic_vector;
        b : std_logic_vector;
        c : std_logic_vector;
    end record;

    type output_transaction_t is record
        r : std_logic_vector;
    end record;

end package;
