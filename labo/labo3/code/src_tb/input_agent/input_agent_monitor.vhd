library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library tlmvm;
context tlmvm.tlmvm_context;

use work.transactions_pkg.all;
use work.math_computer_pkg.all;

entity input_agent_monitor is
    generic (
        DATASIZE : integer := 8;
        procedure blocking_put(variable input_transaction : input_transaction_t)
        );
    port (
        clk         : in std_logic;
        rst         : in std_logic;
        port_input  : in math_input_itf_in_t;
        port_output : in math_input_itf_out_t
        );
end input_agent_monitor;

architecture testbench of input_agent_monitor is

begin

    process is
        variable transaction : input_transaction_t(a(DATASIZE-1 downto 0),
                                                   b(DATASIZE-1 downto 0),
                                                   c(DATASIZE-1 downto 0));
        variable counter : integer;
        variable ok      : boolean;
    begin

        counter := 0;
        for i in 0 to 9 loop
            report "Monitor waiting for transaction number " & integer'image(counter) severity note;
            ok := false;
            while (not ok) loop
                wait until rising_edge(clk);
                -- TODO : Maybe handle the protocol differently
                transaction.a := port_input.a;
                transaction.b := port_input.b;
                transaction.c := port_input.c;
                blocking_put(transaction);
                report "Monitor received transaction number " & integer'image(counter) severity note;
                counter       := counter + 1;
                ok            := true;
            end loop;
        end loop;

        wait;
    end process;

end testbench;
