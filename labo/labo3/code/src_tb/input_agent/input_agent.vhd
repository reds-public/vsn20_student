library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library tlmvm;
context tlmvm.tlmvm_context;

use work.transactions_pkg.all;
use work.math_computer_pkg.all;


entity input_agent is

    generic (
        DATASIZE : integer := 8;
        TESTCASE : integer := 0;
        procedure blocking_put(variable input_transaction : input_transaction_t)
        );

    port (
        clk_i         : in  std_logic;
        rst_i         : in  std_logic;
        port_input_o  : out math_input_itf_in_t;
        port_output_i : in  math_input_itf_out_t
        );

end input_agent;

architecture testbench of input_agent is

    subtype real_input_transaction_t is input_transaction_t(
        a(DATASIZE-1 downto 0),
        b(DATASIZE-1 downto 0),
        c(DATASIZE-1 downto 0));

    -- Create a specialized FIFO package with the same data width for holding
    -- the transaction (requests and responses) they are of the same type.
    -- Very importan: Use the specialized subtypes, not the generic ones
    package input_transaction_pkg is new tlm_fifo_pkg
    generic map (element_type => real_input_transaction_t,
                 nb_max_data => 1);

    shared variable fifo_seq0_to_driver0 : input_transaction_pkg.tlm_fifo_type;

    procedure blocking_get_input_agent_driver(variable input_transaction : out input_transaction_t) is
    begin
        input_transaction_pkg.blocking_get(fifo_seq0_to_driver0, input_transaction);
    end blocking_get_input_agent_driver;

    procedure blocking_put_input_agent_sequencer(variable input_transaction : in input_transaction_t) is
    begin
        input_transaction_pkg.blocking_put(fifo_seq0_to_driver0, input_transaction);
    end blocking_put_input_agent_sequencer;

    signal port_input_s : math_input_itf_in_t(
        a(DATASIZE-1 downto 0),
        b(DATASIZE-1 downto 0),
        c(DATASIZE-1 downto 0));

begin

    port_input_o <= port_input_s;

    input_agent_sequencer : entity work.input_agent_sequencer
        generic map(DATASIZE     => DATASIZE,
                    TESTCASE     => TESTCASE,
                    blocking_put => blocking_put_input_agent_sequencer);

    input_agent_driver : entity work.input_agent_driver
        generic map(
            DATASIZE     => DATASIZE,
            blocking_get => blocking_get_input_agent_driver)
        port map(clk_i,
                 rst_i,
                 port_input_s,
                 port_output_i);

    input_agent_monitor : entity work.input_agent_monitor
        generic map(
            DATASIZE     => DATASIZE,
            blocking_put => blocking_put)
        port map(clk_i,
                 rst_i,
                 port_input_s,
                 port_output_i);


end testbench;
