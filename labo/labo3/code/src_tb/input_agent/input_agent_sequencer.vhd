library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library tlmvm;
context tlmvm.tlmvm_context;

use work.transactions_pkg.all;
use work.math_computer_pkg.all;

entity input_agent_sequencer is
    generic (
        DATASIZE : integer := 8;
        TESTCASE : integer := 0;
        procedure blocking_put(variable input_transaction : input_transaction_t)
        );
end input_agent_sequencer;

architecture testbench of input_agent_sequencer is

begin

    process is
        variable transaction : input_transaction_t(a(DATASIZE-1 downto 0),
                                                   b(DATASIZE-1 downto 0),
                                                   c(DATASIZE-1 downto 0));
        variable counter : integer;
    begin
        raise_objection;
        counter := 0;

        -- TODO : Obviously these testcases are not totally perfect
        case TESTCASE is
            when 0 =>
                for i in 0 to 9 loop
                    transaction.a := std_logic_vector(to_unsigned(i, transaction.a'length));
                    transaction.b := std_logic_vector(to_unsigned(i + 10, transaction.b'length));
                    transaction.c := std_logic_vector(to_unsigned(i + 20, transaction.c'length));
                    blocking_put(transaction);
                    report "Sequencer : Sent transaction number " & integer'image(counter) severity note;
                    counter       := counter + 1;
                end loop;

            when 1 =>
                for i in 0 to 9 loop
                    transaction.a := std_logic_vector(to_unsigned(i, transaction.a'length));
                    transaction.b := std_logic_vector(to_unsigned(i + 20, transaction.b'length));
                    transaction.c := std_logic_vector(to_unsigned(i + 30, transaction.c'length));
                    blocking_put(transaction);
                    report "Sequencer : Sent transaction number " & integer'image(counter) severity note;
                    counter       := counter + 1;
                end loop;

            when others =>
                report "Sequencer : Unsupported testcase" severity error;
        end case;

        drop_objection;
        report "Sequencer finished his job" severity note;
        wait;
    end process;
end testbench;
