library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library tlmvm;
context tlmvm.tlmvm_context;

use work.transactions_pkg.all;
use work.math_computer_pkg.all;

entity input_agent_driver is
    generic (
        DATASIZE : integer := 8;
        procedure blocking_get(variable input_transaction : out input_transaction_t)
        );
    port (
        clk         : in  std_logic;
        rst         : in  std_logic;
        port_input  : out math_input_itf_in_t;
        port_output : in  math_input_itf_out_t
        );
end input_agent_driver;

architecture testbench of input_agent_driver is

begin

    process is
        variable transaction : input_transaction_t(a(DATASIZE-1 downto 0),
                                                   b(DATASIZE-1 downto 0),
                                                   c(DATASIZE-1 downto 0));
        variable counter : integer;
    begin
        -- Example of objection use. Could be handled differently
        raise_objection;

        counter          := 0;
        port_input.valid <= '0';
        for i in 0 to 9 loop
            report "Driver waiting for transaction number " & integer'image(counter) severity note;
            blocking_get(transaction);
            report "Driver received transaction number " & integer'image(counter) severity note;
            wait until falling_edge(clk);
            -- TODO : Manage the protocol
            port_input.a     <= transaction.a;
            port_input.b     <= transaction.b;
            port_input.c     <= transaction.c;
            counter          := counter + 1;
        end loop;

        drop_objection;
        wait;
    end process;

end testbench;
