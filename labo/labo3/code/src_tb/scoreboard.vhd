library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library tlmvm;
context tlmvm.tlmvm_context;

use work.transactions_pkg.all;
use work.math_computer_pkg.all;


entity scoreboard is
    generic (
        DATASIZE : integer := 8;
        TESTCASE : integer := 0;
        procedure blocking_get_input(variable input_transaction   : out input_transaction_t);
        procedure blocking_get_output(variable output_transaction : out output_transaction_t)
        );
end scoreboard;


architecture testbench of scoreboard is
begin

    proc_scoreboard : process
        variable trans_input : input_transaction_t(
            a(DATASIZE-1 downto 0),
            b(DATASIZE-1 downto 0),
            c(DATASIZE-1 downto 0));
        variable trans_output : output_transaction_t(r(DATASIZE-1 downto 0));
        variable counter      : integer;
        variable expected     : std_logic_vector(DATASIZE-1 downto 0);
    begin

        -- Example of objection raised. Maybe not the best option...
        raise_objection;

        counter := 0;

        for i in 0 to 9 loop
            report "Scoreboard waiting for transaction number " & integer'image(counter) severity note;
            blocking_get_output(trans_output);
            blocking_get_input(trans_input);
            report "Scoreboard received transaction number " & integer'image(counter) severity note;
            -- TODO : Check if the output corresponds to the expected

            counter := counter + 1;
        end loop;

        drop_objection;

        wait;

    end process;

end testbench;
