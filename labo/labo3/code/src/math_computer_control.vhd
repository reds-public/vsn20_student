library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use work.math_computer_pkg.all;

entity math_computer_control is
    generic (
        ERRNO : integer := 0
        );
    port (
        clk_i                 : in  std_logic;
        rst_i                 : in  std_logic;
        control_to_datapath_o : out control_to_datapath_t;
        datapath_to_control_i : in  datapath_to_control_t;
        start_i               : in  std_logic;
        ready_i               : in  std_logic;
        valid_o               : out std_logic;
        ready_o               : out std_logic
        );
end math_computer_control;

architecture behave of math_computer_control is

    type state_t is (sInit, s1, s2, s3, s4);

    signal state_s        : state_t;
    signal next_state_s   : state_t;
    signal counter_s      : integer;
    signal next_counter_s : integer;

begin

    process(all) is
        -- 2 seeds pour la génération aléatoire
        variable seed1, seed2 : positive;
        -- valeur aléatoire entre 0 et 1.0
        variable rand         : real;
        -- valeur aléatoire entre 0 et 65535
        variable int_rand     : integer;

    begin
        ready_o <= '0';
        valid_o <= '0';

        control_to_datapath_o.sel0       <= "000";
        control_to_datapath_o.sel1       <= "000";
        control_to_datapath_o.sel2       <= "000";
        control_to_datapath_o.sel3       <= "000";
        control_to_datapath_o.sel_output <= "000";

        next_state_s   <= state_s;
        next_counter_s <= counter_s;

        case state_s is
            when sInit =>
                if ERRNO /= 3 then
                    ready_o <= '1';
                end if;
                if (start_i = '1') then
                    -- we load the input values
                    control_to_datapath_o.sel0 <= "001";
                    control_to_datapath_o.sel1 <= "010";
                    if ERRNO = 2 then
                        control_to_datapath_o.sel1 <= "011";
                    end if;
                    control_to_datapath_o.sel2 <= "011";
                    next_state_s               <= s1;
                end if;

            when s1 =>
                control_to_datapath_o.sel1 <= "100";
                next_state_s               <= s2;

            when s2 =>
                control_to_datapath_o.sel1 <= "100";
                next_state_s               <= s3;


            when s3 =>
                control_to_datapath_o.sel3 <= "101";
                next_state_s               <= s4;
                UNIFORM(seed1, seed2, rand);
                int_rand                   := integer(TRUNC(rand*20.0));
                next_counter_s             <= 0;

            when s4 =>
                if (counter_s >= int_rand) then
                    control_to_datapath_o.sel_output <= "011";
                    if ERRNO /= 1 then -- or ((counter_s mod 2) = 0)then
                        valid_o <= '1';
                    end if;
                    if (ready_i = '1') then
                        next_state_s <= sInit;
                    end if;
                else
                    next_counter_s <= counter_s + 1;
                end if;
        end case;

    end process;

    process(clk_i, rst_i) is
    begin
        if rst_i = '1' then
            state_s   <= sInit;
            counter_s <= 0;
        elsif rising_edge(clk_i) then
            state_s   <= next_state_s;
            counter_s <= next_counter_s;
        end if;
    end process;

end behave;
