
# Install the pre compiled OSVVM library in Questasim folder
cp -r osvvm_compiled /opt/mentor/questasim/osvvm

# Get the sources
git clone https://github.com/OSVVM/OSVVM.git
git clone --recursive https://github.com/OSVVM/VerificationIP.git
